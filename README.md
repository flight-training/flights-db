# Dynamo DB

## Pull image
```
docker pull amazon/dynamodb-local
```

## Start docker container
```
docker run -p 8000:8000 amazon/dynamodb-local
```

## Create table, list tables, scan table

```
aws configure
```

Enter the following values:
```
AWS Access Key ID [None]: foo
AWS Secret Access Key [None]: bar
Default region name [None]: local
Default output format [None]: json
```

```
aws dynamodb create-table --cli-input-json file://config/tables/tables.json --endpoint-url http://localhost:8000
aws dynamodb list-tables --endpoint-url http://localhost:8000
aws dynamodb scan --table-name "flights-dev" --endpoint-url http://localhost:8000
```

based on this awesome blog post:
https://medium.com/@Keithweaver_/using-aws-dynamodb-using-node-js-fd17cf1724e0

